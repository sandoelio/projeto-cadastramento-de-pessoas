<?php

	include_once("conexao.php");

// funcao que serve para consultar atraves de um parametro ou nao 
// se for passado um parametro retorna a linha solicitada caso nao retorna todos so dados	
	function consultar($id = null){
		$conexao = get_conexao();
		$sql = "SELECT
					p.id AS id_pessoa, p.nome AS nome_pessoa, p.email ,p.cpf, p.observacao,
					c.nome AS nome_cidade, e.sigla AS sigla_estado,
					c.id AS id_cidade, e.nome AS nome_estado
				FROM 
					pessoa p
				INNER JOIN cidade c ON p.id_cidade = c.id
				INNER JOIN estado e ON c.id_estado = e.id";

		if ($id == null) {
			return $conexao->query($sql);
		}
// nesta linha sera retornado por registro solicitado 	
		$sql  = $sql . " WHERE p.id = ?";
		$stmt = $conexao->prepare($sql);
		$stmt->execute(array($id));
		return $stmt->fetch();
	}

	

	function inserir($nome, $email, $cpf, $observacao, $cidade){

		$conexao  = get_conexao();
		$cadastro = $conexao->prepare("INSERT INTO pessoa (nome, email, cpf,observacao, id_cidade)      
									  VALUES (:nome, :email, :cpf, :observacao, :cidade)");
		$cadastro->bindParam(":nome", $nome);
		$cadastro->bindParam(":email", $email);
		$cadastro->bindParam(":cpf", $cpf);
		$cadastro->bindParam(":observacao", $observacao);
		$cadastro->bindParam(":cidade", $cidade);
		return $cadastro->execute();
		
	}

	function cpfJaCadastrado($cpf){

		$conexao     = get_conexao();	
// seleciona o cpf no banco		
		$sql_validar = $conexao->query("SELECT cpf FROM pessoa WHERE cpf = '$cpf'");
//contando o numero de linhas	
        $quantidadeRegistros = $sql_validar->rowCount(); 		
// se for igual a zero nao existe registro com esse cpf   
		if ($quantidadeRegistros == 0) {
			return true;
		}
       	return false;
	}

	function excluir($id){
        $conexao     = get_conexao();
	    $sql_deletar = $conexao->prepare("DELETE FROM pessoa WHERE id = :id");
        $sql_deletar->bindParam(":id" , $id);
        return $sql_deletar->execute();
	}

	function getEstados(){
		$conexao = get_conexao();
		return $conexao->query("SELECT id, nome, sigla FROM estado ORDER BY sigla");
	}

	function getCidade($idEstado){
		$conexao = get_conexao();
		$sql = "SELECT id, nome FROM cidade where id_estado = ? order by nome";	
		$stmt = $conexao->prepare($sql);
		$stmt->execute(array($idEstado));
		return $stmt->fetchAll();	
	}

	function atualizarCadastro($id, $nome, $email, $cpf, $observacao, $cidade){
	    $conexao   = get_conexao();
		$atualizar = $conexao->prepare('UPDATE pessoa SET nome= :nome, email= :email, cpf= :cpf, observacao= :observacao, id_cidade= :cidade WHERE id= :id');	
		$atualizar->bindParam(":id", $id);
		$atualizar->bindParam(":nome", $nome);
		$atualizar->bindParam(":email", $email);
		$atualizar->bindParam(":cpf", $cpf);
		$atualizar->bindParam(":observacao", $observacao);
		$atualizar->bindParam(":cidade", $cidade);		
		return $atualizar->execute();

	}  
// consulta direcionada a edição no proprio principal
	function consultaEditar($update){	
		$conexao = get_conexao();
		
		$sql = "SELECT
					p.id AS id_pessoa, p.nome AS nome_pessoa, p.email ,p.cpf, p.observacao,
					c.nome AS nome_cidade, e.sigla AS sigla_estado,
					c.id AS id_cidade, e.nome AS nome_estado,e.id AS id_estado
				FROM 
					pessoa p
				INNER JOIN cidade c ON p.id_cidade = c.id
				INNER JOIN estado e ON c.id_estado = e.id
				WHERE p.id = ?";
		$stmt = $conexao->prepare($sql);
		$stmt->execute(array($update));
		return $stmt->fetch();
		
	}
   
?>