 <?php

 include_once("funcoes.php");

//verificando se o campo oculto e falso

$form_edicao  = 0;

 //verificando se OPCAO em editar existe 

if (isset($_GET['opcao']) && $_GET['opcao'] == 'editar') {
//pega o id via url e adiciona na variavel
	$id = $_GET['id'];	
//captura o id na url e adiciona na variavel no campo oculto do form	
	$form_id  = $_GET['id'];
//passando o parametro ID para a funcao
    $up = consultar($id);

//verificando se estar vazia e imprimi os valores do form se estiver realizando o UPDATE
   
	if (!empty($up)) {		
	    $form_nome 			= $up['nome_pessoa'];
	    $form_email			= $up['email'];
	    $form_cpf 			= $up['cpf'];
	    $form_observacao 	= $up['observacao'];
	    $form_nome_estado	= $up['nome_estado'];
	    $form_nome_cidade	= $up['nome_cidade'];
	    $form_id_cidade   	= $up['id_cidade'];
// setando o campo oculto como verdadeiro	    
	    $form_edicao 		= 1; 
	}

 }

//verificando se a entrada de dados estar vazia ao INSERIR
if (!empty($_POST)) {
// afirmando que o form e verdadeiro	
	$formValido = true;

	if (trim($_POST["nome"])==""){
		echo "* o campo nome em branco <br/>";
		$formValido = false;
	}

	if (trim($_POST["email"])==""){
		echo "* o campo email em branco <br/>";
		$formValido = false;
	}     
	if (trim($_POST["cpf"])==""){
		echo "* o campo cpf em branco <br/>";
		$formValido = false;
	}
	if (trim($_POST["observacao"])=="") {
		echo "* o campo observacao em branco <br/>";
		$formValido = false;
	}
// verificando se o cpf estar cadastrado 
// trim--> elimina espaços antes e depois
// afirmando que form e TRUE e adicionando os dados do form na variavel	
	if ($formValido) {
		$nome 		=trim($_POST["nome"]);  
		$email 		=trim($_POST["email"]);			
		$observacao =trim($_POST["observacao"]);
		$cidade 	= $_POST['cmbCidade'];	
		$cpf 		= trim($_POST["cpf"]);	
// se a acao for uma edicao recebe o valor do ID do banco na variavel e atualiza senao se caso for inserir consulta o cpf 
		if ($_POST['edicao']) {
			$id = $_POST['id'];
			
			$result = atualizarCadastro($id, $nome, $email, $cpf, $observacao, $cidade);
			
		} else {
			$result2 = cpfJaCadastrado($cpf);
			if ($result2) {	
				$result = inserir($nome, $email, $cpf, $observacao, $cidade);
			}else{
		    	echo'<center>Cpf ja existe</center>';
		    } 

		}

			if ($result){
			    header("location:principal.php");
			}else{
				echo '<center><h2>Erro ao inserir no banco</h2></center>';  
			}
    } 
}
	
?>
	
<!DOCTYPE html>

<html>

<head>

	<title>Tela de cadastro</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="estilo.css" rel="stylesheet" type="text/css" />

</head>

<body class="body">

	<form action="cad.php" method="POST" name="form" id="frmContato">
		
		<label for="nome">
 <!--existe esta variavel? imprima o valor ou vazio-->
		Nome: <input type="text" name="nome" id="nome" value="<?= isset($form_nome) ? $form_nome : '';   ?>" />
		</label>
		

		<label for="email">
		E-mail: <input type="text" name="email" id="email" value="<?= isset($form_email) ? $form_email : '';   ?>" />
		</label>

		<label for="cpf">
		Cpf: <input type="text" name="cpf" id="cpf" value="<?= isset($form_cpf) ? $form_cpf : ''; ?>" />
		</label>
<!--campos criado para controle de validacao do update-->
		<input type="hidden" name="edicao" id= "edicao" value ="<?= $form_edicao; ?>">	
		<input type="hidden" name="id" id= "id" value ="<?= $form_id; ?>">		

		Observacao:
		<textarea name="observacao" id="observacao"><?= isset($form_observacao) ? $form_observacao : ''; ?></textarea>

		<label for="estado" >Estado
			 <select id="cmbEstado" name="cmbEstado" value="" >
				 <option value="" selected="selected"> <?= isset($form_nome_estado) ? $form_nome_estado : 'selecione'; ?></option>
					<?php

	     			//chamando a funcao 
	                $resultEstados = getEstados();  

	                // pegando o resultado da funcao e adicionando em $codigos
	                $codigos = $resultEstados->fetchAll();

	                //percorre os dados e adiciona em outra variavel
	                foreach($codigos as $item) {
					?> 
						<option value="<?= $item['id']; ?>" > <?= $item['nome']; ?></option>;						    
		            <?php
		            }	                
			        ?>
			 </select>
		 </label>

		 <label for="cidade">Cidade
			 <select  name="cmbCidade" id="cmbCidade" >
				 <option id="cmbCidades" value=" <?= isset($form_id_cidade) ? $form_id_cidade : ''; ?>"> <?= isset($form_nome_cidade) ? $form_nome_cidade : ''; ?></option>
			 </select>
		 </label>

		<input type="submit" value="Enviar" id="submit" class="botao"/>
		<input type="reset" value="resetar" class="botao"/>

	</form>
  </body>
</html>
<script type="text/javascript">

	$(document).ready(function(){
        // quando selecionar a opcao estado o CHANGE modifica o combo cidade
		$("#cmbEstado").change(function(e){ 
			//seleciona no combo estado o valor da opcao e atribui ao id_estado
			var id_estado = $("#cmbEstado option:selected").val();
	        //buscando o arquivo consultar e passando os parametos opcao e valor retornado na function (dados)                          
			$.getJSON('consultar.php?opcao=cidade&valor='+id_estado, function (dados){
				if (dados.length > 0){ 	
					var option = '<option>Selecione...</option>';
				    $.each(dados, function(i, obj){
				    	option += '<option value="'+obj.id+'">'+obj.nome+'</option>';
					});
				
				}
				$('#cmbCidade').html(option);

			});

		})	
			$ (this) .remove ();  
});

</script>

