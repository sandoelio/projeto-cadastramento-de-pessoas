<?php

include_once("funcoes.php");

// condicao para exclusao do registro
if (isset($_GET["id"]) && !empty($_GET["id"])) {
  $result =  excluir($_GET["id"]);
	 if ($result){
	  	echo 'excluido com sucesso';
	 }else{
	  	echo 'Erro ao excluir';
	 }  

} 


?>

<!DOCTYPE html>
<html>
    <head>
    	<link href="estilo2.css" rel="stylesheet" type="text/css" />
    	<link href="estilo3.css" rel="stylesheet" type="text/css" />
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    	
	    <title>Principal</title>
    </head>
  <body class="corpo">

	    <form > 
<!--cabecalho da tabela de visualizacao-->
              	<table class="bordas">
		                <tr class="negrito">
				            <td >Nome</td>
				            <td >Email</td>
				            <td >Cpf</td>				         
				            <td >Observacao</td>
				            <td >Cidade</td>
				            <td >Estado</td>
		                </tr>
			               <?php
// variavel recebendo a consulta para imprimir os dados                            
			                $selBd = consultar();
                           
// imprimindo dados da tabela                  
                 while($linha = $selBd->fetch(PDO::FETCH_ASSOC)){
     
				?> 	 
					    <tr>								    	
			             	<td><?= $linha["nome_pessoa"]; ?></td> 
				            <td><?= $linha["email"]; ?></td>
				            <td><?= $linha["cpf"]; ?></td>
				            <td><?= $linha["observacao"]; ?></td>
				            <td><?= $linha["nome_cidade"]; ?></td>
				            <td><?= $linha["sigla_estado"]; ?></td>
				            <td><a href=<?= 'principal.php?id='.$linha["id_pessoa"]; ?> > Excluir </a></td> 				             
				            <td><a href="#" onclick="editar(<?= trim($linha["id_pessoa"]); ?>)" > Editar </a></td>           
				        </tr>
	            <?php

	                }
		        ?>
				</table>
               				<input id="btnVoltar" type=button onClick="parent.location='cad.php'" value='Voltar'>
        </form> 

<!--tabela de edicao-->

	         <div id="divTable" style="display: none">

				<table id="table" class="xxx" >
		                <tr class="negrito">
				            <td >Nome</td>
				            <td >Email</td>
				            <td >Cpf</td>				         
				            <td >Observacao</td>
				            <td >Estado</td>
				            <td >Cidade</td> 
		                </tr> 
			               	 
						<tr>
															    	
			             	<td>
			             		<label for="nome">Nome: 
			             			<input type="text" name="nome" id="nome" value=""/>
			             		</label>
			             	</td>

			             	<td>
			             		<label for="email">E-mail: 
			             			<input type="text" name="email" id="email" value=""/>
			             		</label>
			             	</td>

				            <td>
				            	<label for="cpf">Cpf: 
				            		<input type="text" name="cpf" id="cpf" value="" />
				            	</label>
				            </td>

				            <td>
				            	<textarea name="observacao" id="observacao"></textarea>
				            </td>
				            <td>
				            	<label for="estado" >
				            		<select id="cmbEstado" name="cmbEstado" value="">
				            			<!-- <option value="" selected="selected"></option> -->
										<?php
							                $resultEstados = getEstados();  
							                $codigos = $resultEstados->fetchAll();
								        foreach($codigos as $item) {
										?> 
											<option value="<?= $item['id']; ?>" > <?= $item['sigla']; ?></option>;						    
					            		<?php
				          				  }	                
					       				?>
								    </select>
								</label>
				            </td>
				            <td>
				            	<label for="cidade">
				            		<select  name="cmbCidade" id="cmbCidade"></select>
								</label>
				            </td>
				            <td>
				            	<a href="#" onclick="atualizarTabela('id_pessoa')" > Atualizar </a>				            	
				            </td>
				           
			             		
			             			<input type="hidden" name="hidden_id_pessoa" id="hidden_id_pessoa" value=""/>
			             		
						</tr>
				</table>
	         </div>
   </body>

</html>

<script type="text/javascript">

	function getComboCidade(id_estado, id_cidade_selecionada) {
		$.getJSON('consultar.php?opcao=cidade&valor='+id_estado, function (dados){
			if (dados.length > 0){ 	
				var option = '<option>Selecione...</option>';
			    $.each(dados, function(i, obj){
			    	option += '<option value="'+obj.id+'">'+obj.nome+'</option>';
				});

			}								

			$('#cmbCidade').html(option);
			
			if (id_cidade_selecionada) {
				$('#cmbCidade option')
					.filter('[value='+id_cidade_selecionada+']')
					.attr('selected',true);							
			}
			
		});
	}

	$("#cmbEstado").change(function(e){						 			
		var id_estado = $("#cmbEstado option:selected").val();                         
		getComboCidade(id_estado);
    })									   

   function editar(id_pessoa){

	   	$.ajax({	   				
				type:'POST',
				//Definimos o método HTTP usado					
				url: 'consulta_Jquery.php',
				//Definindo o arquivo onde serão buscados os dados
				data: "param=" + id_pessoa,
				datatype: 'json',
				success: function(data){
				//convertendo json para o objeto js
					data = JSON.parse(data); 

                   $("#hidden_id_pessoa").val(data.id_pessoa);
				   $("#nome").val(data.nome_pessoa); 
				   $("#email").val(data.email); 
				   $("#cpf").val(data.cpf);
				   $("#observacao").val(data.observacao);
				   $("#cmbCidade").val(data.nome_cidade);  
				   $('#cmbEstado option')
				     .removeAttr('selected')
				     .filter('[value='+data.id_estado+']')
				     .attr('selected', true);

					getComboCidade(data.id_estado, data.id_cidade);
			},
				error: function (error) {
				    alert('error; ' + eval(error));
				}
	});

		$("#divTable").show();
    }		

	function atualizarTabela(id_pessoa){

		var p = {
		    'id_pessoa': $("#hidden_id_pessoa").val(),
			'nome':      $("#nome").val(),
			'email':     $("#email").val(),
			'cpf':       $("#cpf").val(),
			'observacao':$("#observacao").val(),
			'cmbCidade': $("#cmbCidade").val(),
			'cmbEstado': $("#cmbEstado").val(),	
		}
         
        $.ajax({
        	type:'POST',
        	url:'atualizar.php',
        	data: p,       	
        	success: function(data){		
        	console.log(data);	
        	if(data){
       		  alert("Cadastrado com Sucesso!");
	   		  window.location.href = "principal.php";    		              
            }else {
              alert('Dados nao cadastrado');
            }

            },
        	  error: function (error) {
				  alert('error; ' + eval(error));
			  }
        });

   }
</script>